package com.daioware.music;

public interface MultimediaLibrary extends Iterable<ArtistItem> {
	boolean loadLibrary();
	boolean saveLibrary();
}
