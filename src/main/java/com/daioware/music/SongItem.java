package com.daioware.music;

import java.io.File;

import com.daioware.commons.NameItem;

public interface SongItem extends NameItem{
	String getLyrics();
	File getFile();
}
